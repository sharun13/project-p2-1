const array = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]

// people who are Agender

let agenderPeople = array.filter((people) => {
    if (people.gender === "Agender") {
        return true;
    }
});

console.log(agenderPeople);

// Split IP address into their components

let splitIP = array.map((item) => {
    item.ip_address = (item.ip_address.split('.').map(Number))
    return item;
});

console.log(splitIP);

// Sum of second components of the IP addrss

let sumOfSecondComponentOfIP = splitIP.reduce((accumulator, current) => {
    accumulator += current.ip_address[1];
    return accumulator;
}, 0);

console.log(sumOfSecondComponentOfIP);

// Sum of fourth components of the IP addrss

let sumOfFourthComponentOfIP = splitIP.reduce((accumulator, current) => {
    accumulator += current.ip_address[3];
    return accumulator;
}, 0);

console.log(sumOfFourthComponentOfIP);

// Find the full name of each person

let fullName = array.map((person) => {
    person.full_name = person.first_name + " " + person.last_name;
    return person;
});

console.log(fullName);

// Filter out all the .org emails

let peopleWithDotOrgEmail = array.filter((person) => {
    if (person.email.endsWith('org')) {
        return true;
    }
});

console.log(peopleWithDotOrgEmail);

// number of emails 

let extensions = {};
array.filter((person) => {
    let mailDomain = person.email.split('.').slice(-1);
    extensions[mailDomain] = (extensions[mailDomain] || 0) + 1;
    return true;
});
console.log(extensions);

// Sort the data according to the first name

let sortedData = array.sort((firstValue, secondValue) => {
    if (firstValue.first_name < secondValue.first_name) {
        return 1;
    } else {
        return -1;
    }
});

console.log(sortedData);

